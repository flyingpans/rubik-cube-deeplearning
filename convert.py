import tensorflow as tf
import os
import sys
import numpy as np
import argparse
import csv
import cube as cb

FLAGS=None

def _float_feature(value):
    """Wrapper for inserting an float Feature into a SequenceExample proto,
    """
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))

def _int64_feature(value):
    """Wrapper for inserting an int64 Feature into a SequenceExample proto,
    e.g, An integer label.
    """
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def _bytes_feature(value):
    """Wrapper for inserting a bytes Feature into a SequenceExample proto,
    e.g, an image in byte
    """
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def _int64_feature_list(values):
    """Wrapper for inserting an int64 FeatureList into a SequenceExample proto,
    e.g, sentence in list of ints
    """
    return tf.train.FeatureList(feature=[_int64_feature(v) for v in values])

def _bytes_feature_list(values):
    """Wrapper for inserting a bytes FeatureList into a SequenceExample proto,
    e.g, sentence in list of bytes
    """
    return tf.train.FeatureList(feature=[_bytes_feature(v) for v in values])

def main(unused_argv):
    writer = tf.python_io.TFRecordWriter(FLAGS.sample + ".tfrecord")
    with open(FLAGS.sample + ".txt") as f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            if len(row) != cb.Cube.cubes + 2:
                continue
            sts = list(map(int, row[:-2]))
            stp = list(map(int, row[-2:-1]))
            rew = list(map(float, row[-1:]))
            feature = {
              'training/cube': tf.train.Feature(
                  int64_list=tf.train.Int64List(value=sts)),
              'training/step': _int64_feature(stp[0]),
              'training/reward': _float_feature(rew[0]) }
            example = tf.train.Example(features = tf.train.Features(feature = feature))
            writer.write(example.SerializeToString())
    writer.close()
    sys.stdout.flush()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
            '--sample', type = str,
            default = '',
            help = 'sample file')
    FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main = main, argv = [sys.argv[0]] + unparsed)
