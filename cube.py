import numpy as np
import csv
import settings

class Cube:
    size = 3
    corner_cubes  = 8
    edge_cubes    = 12
    corner_colors = 3
    edge_colors   = 2
    nencodings    = 24
    cubes         = corner_cubes + edge_cubes
    nstates = (corner_cubes * corner_cubes * corner_colors + \
            edge_cubes * edge_cubes * edge_colors)
    rot_corner_loc    = dict()
    rot_edge_loc      = dict()
    rot_corner_orient = dict()
    rot_edge_orient   = dict()
    color_code_corner = ['yrg', 'ybr', 'yob', 'ygo', 'wgr', 'wrb', 'wbo', 'wog']
    color_code_edge   = [
            'yg', 'yr', 'yb', 'yo', 'rg', 'rb', 'ob', 'og',
            'wg', 'wr', 'wb', 'wo']
    spc = (-1, ' ', 0)
    order = [
            [spc, spc, spc, (0,2,0), (1,2,0),    (0,1,0), spc, spc, spc],
            [spc, spc, spc, (1,3,0), (-1,'y',0), (1,1,0), spc, spc, spc],
            [spc, spc, spc, (0,3,0), (1,0,0),    (0,0,0), spc, spc, spc],
            [(0,2,1), (1,3,1), (0,3,2), (0,3,1), (1,0,1), (0,0,2), (0,0,1),
                (1,1,1), (0,1,2), (0,1,1), (1,2,1), (0,2,2)],
            [(1,6,0), (-1,'o',0), (1,7,0), (1,7,1), (-1,'g',0), (1,4,1),
                (1,4,0), (-1,'r',0), (1,5,0), (1,5,1), (-1,'b',0), (1,6,1)],
            [(0,6,2), (1,11,1), (0,7,1), (0,7,2), (1,8,1), (0,4,1), (0,4,2),
                (1,9,1), (0,5,1), (0,5,2), (1,10,1), (0,6,1)],
            [spc, spc, spc, (0,7,0), (1,8,0), (0,4,0), spc, spc, spc],
            [spc, spc, spc, (1,11,0), (-1,'w',0), (1,9,0), spc, spc, spc],
            [spc, spc, spc, (0,6,0), (1,10,0), (0,5,0), spc, spc, spc]]
    def load_turn_table():
        table_list = [
            Cube.rot_corner_loc,
            Cube.rot_edge_loc,
            Cube.rot_corner_orient,
            Cube.rot_edge_orient]
        with open('rubik-turn-table.txt', 'r') as f:
            reader = csv.reader(f, delimiter=',')
            i = 0
            act = 0
            for row in reader:
                if (len(row) == 0):
                    continue
                table_list[i][settings.actions[act]] = list(map(int, row))
                act += 1
                if act == len(settings.actions):
                    i += 1
                    act = 0
    def solved(self):
        if not np.array_equal(self.corner_loc, np.arange(self.corner_cubes)):
            return False
        if not np.array_equal(self.corner_orient, np.zeros(self.corner_cubes,
            dtype=np.int32)):
            return False
        if not np.array_equal(self.edge_loc, np.arange(self.edge_cubes)):
            return False
        if not np.array_equal(self.edge_orient,
                np.zeros(self.edge_cubes, dtype=np.int32)):
            return False
        return True
    def reset(self):
        self.corner_loc = np.arange(self.corner_cubes)
        self.corner_orient = np.zeros(self.corner_cubes, dtype = np.int32)
        self.edge_loc = np.arange(self.edge_cubes)
        self.edge_orient = np.zeros(self.edge_cubes, dtype = np.int32)
    def clone(self):
        x = Cube()
        x.corner_loc = np.copy(self.corner_loc)
        x.corner_orient = np.copy(self.corner_orient)
        x.edge_loc = np.copy(self.edge_loc)
        x.edge_orient = np.copy(self.edge_orient)
        return x
    def encode(self):
        enc = np.zeros(self.corner_cubes + self.edge_cubes, dtype = np.int64)
        for i,l in enumerate(self.corner_loc):
            enc[i] = l * self.corner_colors + self.corner_orient[i]
        for _i,l in enumerate(self.edge_loc):
            i = _i + self.corner_cubes
            enc[i] = l * self.edge_colors + self.edge_orient[_i]
        return enc
    def __init__(self):
        if not Cube.rot_corner_loc:
            Cube.load_turn_table()
        self.reset()
    def turn(self, turn_code):
        for t in turn_code:
            self.corner_loc = self.corner_loc[self.rot_corner_loc[t]]
            self.corner_orient = self.corner_orient[self.rot_corner_loc[t]]
            self.edge_loc = self.edge_loc[self.rot_edge_loc[t]]
            self.edge_orient = self.edge_orient[self.rot_edge_loc[t]]
            self.corner_orient = (self.corner_orient +
                    self.rot_corner_orient[t]) % self.corner_colors
            self.edge_orient = (self.edge_orient +
                    self.rot_edge_orient[t]) % self.edge_colors
    def print(self):
        for order in self.order:
            print()
            for o in order:
                if o[0] == -1:
                    p = o[1]
                elif o[0] == 0:
                    idx = self.corner_loc[o[1]]
                    c = self.color_code_corner[idx]
                    p = c[(self.corner_orient[o[1]] + o[2]) %self.corner_colors]
                elif o[0] == 1:
                    idx = self.edge_loc[o[1]]
                    c = self.color_code_edge[idx]
                    p = c[(self.edge_orient[o[1]] + o[2]) % self.edge_colors]
                else:
                    assert(False)
                print(p, end='')
        print()

if __name__ == "__main__":
    cube = Cube()
    cube.turn('ufbFUL')
    cube.print()
