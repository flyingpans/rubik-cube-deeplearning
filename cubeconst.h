
#pragma once

const char actions[] = {
	'F', 'f', 'B', 'b', 'L', 'l', 'R', 'r', 'D', 'd', 'U', 'u'};
const int nactions = sizeof(actions) / sizeof(char);
const int corner_cubes  = 8;
const int edge_cubes    = 12;
const int corner_colors = 3;
const int edge_colors   = 2;
const int nencodings    = 24;
constexpr int cubes     = corner_cubes + edge_cubes;
constexpr int nstates   = (corner_cubes * corner_cubes * corner_colors +
		edge_cubes * edge_cubes * edge_colors);
