#include <iostream>
#include <string>
#include <cstdint>
#include <cstddef>
#include <cstdlib>
#include <cassert>
#include <random>
#include <functional>
#include <limits>
#include <cstring>
#include <csv-parser.h>
#include <algorithm>
#include <cubes.h>
#include <cubeconst.h>

using namespace std;

int rot_corner_loc[nactions][corner_cubes];
int rot_edge_loc[nactions][edge_cubes];
int rot_corner_orient[nactions][corner_cubes];
int rot_edge_orient[nactions][edge_cubes];

void
build_turn_table(const string &turn_table_name)
{
	ifstream turn_table_f(turn_table_name);
	aria::csv::CsvParser parser(turn_table_f);
	struct {
		int *tables;
		size_t sz;
	} tables [] = {
		{ (int *)rot_corner_loc,    sizeof(rot_corner_loc)    },
		{ (int *)rot_edge_loc,      sizeof(rot_edge_loc)      },
		{ (int *)rot_corner_orient, sizeof(rot_corner_orient) },
		{ (int *)rot_edge_orient,   sizeof(rot_edge_orient)   }};

	size_t row_size = 0;
	int table_no = -1;
	int *pdst = nullptr, *pend = nullptr;
	for (auto &row : parser) {
		if (pend == nullptr || pend == pdst) {
			table_no ++;
			row_size = tables[table_no].sz / sizeof(int) / nactions;
			pdst = tables[table_no].tables;
			pend = pdst + row_size * nactions;
		}
		if (!row.empty()) {
			transform(row.begin(), row.end(), pdst,
				[](auto &x) {return stoi(x);});
			pdst += row_size;
		}
	}
}

void Cube::clone(const Cube& r)  {
	corner_loc = r.corner_loc;
	edge_loc = r.edge_loc;
	corner_orient = r.corner_orient;
	edge_orient = r.edge_orient;
}

Cube::Cube() : corner_orient(corner_cubes, 0), edge_orient(edge_cubes, 0) {
	corner_loc.resize(corner_cubes);
	iota(corner_loc.begin(), corner_loc.end(), 0);
	edge_loc.resize(edge_cubes);
	iota(edge_loc.begin(), edge_loc.end(), 0);
}

bool Cube::solved() const {
	int expected = -1;
	if (!all_of(corner_loc.begin(), corner_loc.end(),
		[&](int i) {expected ++;return expected == i;}))
		return false;
	expected = -1;
	if (!all_of(edge_loc.begin(), edge_loc.end(),
		[&](int i) {expected ++;return expected == i;}))
		return false;
	if (!all_of(corner_orient.begin(), corner_orient.end(),
				[](int i) { return i == 0; }))
		return false;
	if (!all_of(edge_orient.begin(), edge_orient.end(),
				[](int i) { return i == 0; }))
		return false;
	return true;
}

void Cube::turn(int t) {
	assert(t < nactions);
	vector<int> tmp;
	for (auto i = 0; i < corner_loc.size(); i ++)
		tmp.push_back(corner_loc[rot_corner_loc[t][i]]);
	corner_loc = tmp;
	tmp.clear();
	for (auto i = 0; i < corner_orient.size(); i ++) {
		tmp.push_back(corner_orient[rot_corner_loc[t][i]]);
		tmp[i] += rot_corner_orient[t][i];
		tmp[i] %= corner_colors;
	}
	corner_orient = tmp;
	tmp.clear();
	for (auto i = 0; i < edge_loc.size(); i ++)
		tmp.push_back(edge_loc[rot_edge_loc[t][i]]);
	edge_loc = tmp;
	tmp.clear();
	for (auto i = 0; i < edge_orient.size(); i ++) {
		tmp.push_back(edge_orient[rot_edge_loc[t][i]]);
		tmp[i] += rot_edge_orient[t][i];
		tmp[i] %= edge_colors;
	}
	edge_orient = tmp;
}

const vector<int> Cube::encode() {
	vector<int> enc(cubes);
	auto enc_iter = enc.begin();
	for (auto i = 0; i < corner_cubes; i ++)
		*enc_iter++ = corner_colors * corner_loc[i] +
			corner_orient[i];
	for (auto i = 0; i < edge_cubes; i ++)
		*enc_iter++ = edge_colors * edge_loc[i] +
			edge_orient[i];
	return enc;
}

const vector<string> color_code_corner {
	"yrg", "ybr", "yob", "ygo", "wgr", "wrb", "wbo", "wog" };
const vector<string> color_code_edge {
	"yg", "yr", "yb", "yo", "rg", "rb", "ob", "og", "wg", "wr", "wb", "wo"};

struct _order { int type; char c1; char c2; };
#define spc { -1, ' ', 0}
const vector<vector<_order>> orders {
	{spc, spc, spc, {0,2,0}, {1,2,0},    {0,1,0}, spc, spc, spc},
	{spc, spc, spc, {1,3,0}, {-1,'y',0}, {1,1,0}, spc, spc, spc},
	{spc, spc, spc, {0,3,0}, {1,0,0},    {0,0,0}, spc, spc, spc},
	{{0,2,1}, {1,3,1}, {0,3,2}, {0,3,1}, {1,0,1}, {0,0,2}, {0,0,1},
	 {1,1,1}, {0,1,2}, {0,1,1}, {1,2,1}, {0,2,2}},
	{{1,6,0}, {-1,'o',0}, {1,7,0}, {1,7,1}, {-1,'g',0}, {1,4,1},
	 {1,4,0}, {-1,'r',0}, {1,5,0}, {1,5,1}, {-1,'b',0}, {1,6,1}},
	{{0,6,2}, {1,11,1}, {0,7,1}, {0,7,2}, {1,8,1}, {0,4,1}, {0,4,2},
	 {1,9,1}, {0,5,1}, {0,5,2}, {1,10,1}, {0,6,1}},
	{spc, spc, spc, {0,7,0}, {1,8,0}, {0,4,0}, spc, spc, spc},
	{spc, spc, spc, {1,11,0}, {-1,'w',0}, {1,9,0}, spc, spc, spc},
	{spc, spc, spc, {0,6,0}, {1,10,0}, {0,5,0}, spc, spc, spc}
};

void Cube::print() {
	for (auto order : orders) {
		cout << endl;
		char p = 0;
		int index;
		for (auto o : order) {
			switch (o.type) {
			case -1:
				p = o.c1;
				break;
			case 0: {
				index = corner_loc[o.c1];
				auto &c = color_code_corner[index];
				p = c[(corner_orient[o.c1]+o.c2) % corner_colors];
				}
				break;
			case 1:
				{
				index = edge_loc[o.c1];
				auto &c = color_code_edge[index];
				p = c[(edge_orient[o.c1]+o.c2) % edge_colors];
				}
				break;
			default:
				assert(false);
			}
			cout << p;
		}
	}
	cout << endl;
}
