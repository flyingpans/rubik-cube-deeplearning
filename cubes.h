#pragma once
#include <vector>
#include <string>

class Cube {
	private:
		std::vector<int> corner_loc;
		std::vector<int> corner_orient;
		std::vector<int> edge_loc;
		std::vector<int> edge_orient;
		void clone(const Cube& r);
	public:
		Cube();
		Cube(const Cube &rhs) { clone(rhs); }
		Cube & operator=(const Cube &rhs) { clone(rhs); return *this; }
		bool solved() const;
		void turn(int);
		const std::vector<int> encode();
		friend bool operator == (const Cube &, const Cube &);
		friend std::ostream & operator << (std::ostream & os,
				const Cube & c) {
			for (auto x : c.corner_loc)
				os << x << ",";
			for (auto x : c.corner_orient)
				os << x << ",";
			for (auto x : c.edge_loc)
				os << x << ",";
			for (auto x : c.edge_orient)
				os << x << ",";
			return os;
		}
		void print();
};
void build_turn_table(const std::string &);
inline bool operator == (const Cube &l, const Cube &r) {
	return	l.corner_loc == r.corner_loc &&
		l.corner_orient == r.corner_orient &&
		l.edge_loc == r.edge_loc &&
		l.edge_orient == r.edge_orient;
}
