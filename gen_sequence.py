import numpy as np
import cube as cb
import random
import settings

max_sequence_len = 60
prob = 1.0 - 1 / max_sequence_len

def generate_sequence():
    cube = cb.Cube()
    seqno = 0
    states = []
    states.append(cube.encode())
    sequence = []
    for _ in range(max_sequence_len):
        action_index = random.randint(0, len(settings.actions) - 1)
        action = settings.actions[action_index]
        tmp = cube.clone()
        cube.turn(action)
        newstate = cube.encode()
        redo_list = [np.array_equal(state, newstate) for state in states]
        if np.any(redo_list):
            cube = tmp
            continue
        states.append(newstate)
        seqno += 1
        rev_act = action_index // 2 * 2 + (1 - action_index % 2)
        turns = []
        rewards = []
        for x in settings.actions:
            tmp = cube.clone()
            tmp.turn(x)
            if tmp.solved:
                reward = 1
            else:
                reward = -1
            s = tmp.encode()
            turns.append(s)
            rewards.append(reward)
        turns = np.hstack(turns)
        rewards = np.array(rewards, dtype=np.int64)
        sequence.append((newstate, rev_act, seqno, turns, rewards))
    return sequence
