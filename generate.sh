for x in `seq 2`; do
	python simulate.py --tfrecord=newsimulation/simulation-${x}.tfrecord --simulations=1000 --model_dir=./models &
done

wait
