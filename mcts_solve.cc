#include <tensorflow/core/protobuf/meta_graph.pb.h>
#include <tensorflow/core/public/session.h>
#include <tensorflow/core/public/session_options.h>
#include <tensorflow/cc/ops/standard_ops.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdint>
#include <cstddef>
#include <cstdlib>
#include <cassert>
#include <random>
#include <functional>
#include <limits>
#include <cstring>
#include <csv-parser.h>
#include <algorithm>
#include <cxxopts.h>
#include <cubeconst.h>
#include <cubes.h>
#include <memory>
#include <node.h>
#include <thread>
#include <chrono>
#include <map>
#include <iomanip>

using namespace std;
using namespace tensorflow;
using namespace tensorflow::ops;

const string sample_file = "sample";

typedef vector<pair<string, Tensor>> tensor_dict;

int       thinktime          = 10000;
float     exploration_factor = 2.00;
int       nthreads           = 8;
bool      verbose            = false;
int       nsteps             = 10;
int       games              = 1000;
float     value_decay        = 0.9;
string    graph_fn           = "./models/model.pb";

Status LoadModel(Session *sess, string graph_fn, string checkpoint_fn = "")
{
	Status status;

	// Read in the protobuf graph we exported
	GraphDef graph_def;
	status = ReadBinaryProto(Env::Default(), graph_fn, &graph_def);
	if (status != Status::OK()) return status;

	// create the graph in the current session
	status = sess->Create(graph_def);
	if (status != Status::OK()) return status;
	return Status::OK();
}

vector<Cube> generate_one(auto &dice, uint32_t nsteps)
{
	Cube cube;
	vector<Cube> cubes (nsteps);
	for (auto step = 0; step < nsteps; step ++) {
		bool duplicate = true;
		while (duplicate) {
			auto action_index = dice();
			auto tmp = cube;
			cube.turn(action_index);
			duplicate = false;
			for (auto i = 0; i < step; i ++) {
				if (cubes[i] == cube) {
					duplicate = true;
					cube = tmp;
					break;
				}
			}
		}
		cubes[step] = cube;
	}

	return cubes;
}

using rubiknode = node<Cube, nactions>;

pair<bool, float>
expand(Session *session, const Cube &cube, rubiknode &parent)
{
	assert(!parent.expanded());
	auto result = parent.expand_stage_1(cube);
	auto states = parent.get_states();
	auto x = Tensor(DT_INT64, TensorShape({nactions, cubes}));
	auto tensor_x = x.tensor<int64, 2>();
	for (auto i = 0; i < x.dim_size(0); i ++) {
		const auto enc = states[i].encode();
		for (auto cube = 0; cube < cubes; cube ++)
			tensor_x(i, cube) = enc[cube];
	}
	tensor_dict feed_dict = { {"x", x} };
	vector<Tensor> output;
	TF_CHECK_OK(session->Run(feed_dict, {"output"}, {}, &output));
	auto tensor_y = output[0].tensor<float, 1>();
	vector<float> values;
	for (auto i = 0; i < output[0].dim_size(0); i ++) {
		float v = tensor_y(i);
		values.push_back(v);
	}

	return make_pair(result, parent.expand_stage_2(move(values)));
}

pair <bool, float>
mcts_traverse(Session *session, rubiknode &root, vector<Cube *> &path)
{
	if (root.expanded()) {
		auto action = root.selection(path);
		auto &next = root.get_children_nodes()[action];
		auto &states = root.get_states()[action];
		vector<int> tmp;
		tmp.push_back(action);
		reverse(tmp.begin(), tmp.end());
		path.push_back(&states);
		auto result = mcts_traverse(session, next, path);
		auto &maxvalues = root.get_maxvalues();
		if (maxvalues[action] < result.second)
			maxvalues[action] = result.second;
		return result;
	} else
		return expand(session, *path.back(), root);
}

bool check_solution(const vector<int> actions, const Cube &cube, vector<Cube *>
		&seq)
{
	auto cube_copy = cube;
	int i = 0;
	for (auto p : actions) {
		assert(*seq[i] == cube_copy);
		i ++;
		cube_copy.turn(p);
	}
	return cube_copy.solved();
}

pair<bool, rubiknode>
solve_one(Session *session, Cube &cube)
{
	rubiknode root {cube};
	bool solve = false;

	if (cube.solved())
		solve = true;

	if (!solve) {
		for (auto i = 0; i < thinktime; i ++) {
			vector<Cube *> path;
			path.push_back(&cube);
			auto result = mcts_traverse(session, root, path);
			if (result.first) {
				solve = true;
				break;
			}
			if (!verbose || i % 1000 != 0)
				continue;
			cout << "STATS " << i << endl;
			vector<int> stack;
			vector<float> values;
			root.stats(values, stack, 0);
			for (auto j = 0; j < stack.size(); j ++)
				cout << stack[j] << " " <<
				setprecision(3) << values[j] << " * ";
			cout << endl;
			auto &t = root.findbest();
			t.print();
		}
	}
	return make_pair(solve, move(root));
}

auto gen_dice()
{
	default_random_engine generator;
	std::hash<thread::id> hasher;
	generator.seed(chrono::system_clock::now().time_since_epoch().count() +
		       	hasher(this_thread::get_id()));
	uniform_int_distribution<int> distribution(0, nactions - 1);
	return bind(distribution, generator);
}

void evaluate(Session *session, int id, int nsteps, int iter,
    vector<int>::iterator res)
{
	auto dice = gen_dice();
	for (auto i = 0; i < iter; i ++) {
		Cube cube = generate_one(dice, nsteps).back();
		pair<bool, rubiknode> r;
		r = solve_one(session, cube);
		res[id] = res[id] + (r.first ? 1 : 0);
		if (verbose) {
			vector<int> path;
			vector<Cube *>sts;
			auto &root = r.second;
			root.search(path, sts);
			if (r.first) {
				assert(path.size()==sts.size());
				assert(check_solution(path, cube, sts));
			}
			cout << "STATS " << r.first << endl;
			vector<int> stack;
			vector<float> values;
			root.stats(values, stack, 0);
			for (auto i = 0; i < stack.size(); i ++)
				cout << stack[i] << " " << setprecision(3) <<
					values[i] << " * ";
			cout << endl;
		}
	}
}

template<typename... Args>
void task_generator(const string &graph_fn, function<void(Session *, int,
			Args...)> fn, Args... args)
{
	Session *session;
	SessionOptions options;

	TF_CHECK_OK(NewSession(options, &session));
	TF_CHECK_OK(LoadModel(session, graph_fn));

	vector <thread> threads;
	for (auto t = 0; t < nthreads; t ++)
		threads.push_back(thread {fn, session, t, args...});
	for (auto t = 0; t < nthreads; t ++)
		threads[t].join();
}

void do_evaluate()
{
	vector<int> solver_results (nthreads);
	function<void(Session *, int, int, int, vector<int>::iterator)> f =
		evaluate;
	task_generator<int, int, vector<int>::iterator>(
	    graph_fn, f, nsteps, games / nthreads, solver_results.begin());
	auto total = games / nthreads * nthreads;
	auto solved = accumulate(solver_results.begin(),solver_results.end(),0);
	cout << (float)solved / total << endl;
}

void _generate(Session *session, int id, int nsteps, int iter,
		vector<ofstream>::iterator fs)
{
	ofstream &f = fs[id];
	auto dice = gen_dice();

	for (auto i = 0; i < iter; i ++) {
		auto gen_cubes = generate_one(dice, nsteps);
		for (auto step = 0; step < gen_cubes.size(); step ++) {
			auto &cube = gen_cubes[step];
			vector <float> rewards;
			auto x = Tensor(DT_INT64, TensorShape({nactions, cubes}));
			auto tensor_x = x.tensor<int64, 2>();
			for (auto t = 0; t < nactions; t ++) {
				auto tmp = cube;
				tmp.turn(t);
				rewards.push_back(tmp.solved() ? 1.0 : -1.0);
				const auto enc = tmp.encode();
				for (auto c = 0; c < cubes; c ++)
					tensor_x(t, c) = enc[c];
			}
			tensor_dict feed_dict = { {"x", x} };
			vector<Tensor> output;
			TF_CHECK_OK(session->Run(feed_dict, {"output"}, {}, &output));
			auto tensor_y = output[0].tensor<float, 1>();
			float max = numeric_limits<float>::lowest();
			for (auto i = 0; i < output[0].dim_size(0); i ++) {
				float score;
				if (rewards[i] > 0.0)
					score = rewards[i];
				else
					score = rewards[i] + tensor_y(i);
				if (score > max)
					max = score;
			}
			auto enc = cube.encode();
			for (auto v : enc)
				f << int(v) << ",";
			f << step + 1 << ",";
			f << max << endl;
		}
	}
}

void do_generate()
{
	function<void(Session *, int, int, int, vector<ofstream>::iterator)>
		f = _generate;
	vector<ofstream> sample_file_h(nthreads);
	for (auto i = 0; i < nthreads; i ++) {
		ostringstream filename;
		filename << sample_file << i << ".txt";
		sample_file_h[i].open(filename.str());
	}
	task_generator<int, int, vector<ofstream>::iterator>(
		graph_fn, f, nsteps, games / nthreads, sample_file_h.begin());
	for (auto i = 0; i < nthreads; i ++)
		sample_file_h[i].close();
}

map<const string, function<void()>> function_map = {
	{ "evaluate", []() {do_evaluate();} },
	{ "generate", []() {do_generate();} } };

int main(int argc, char **argv)
{
	string run;
	cxxopts::Options opt("solve", "Rubik cube deep learning solver");
	opt.add_options()
		("h,help",  "help messages")
		("t,turns", "number of shuffles", cxxopts::value<int>())
		("g,graph", "locations of the model", cxxopts::value<string>())
		("p,games", "number of games", cxxopts::value<int>())
		("D,value_decay", "decay", cxxopts::value<float>())
		("d,depth", "search depth", cxxopts::value<int>())
		("T,time",  "think time", cxxopts::value<int>())
		("e,expfactor",  "exploration factor", cxxopts::value<float>())
		("v,verbose",  "verbose output", cxxopts::value<bool>())
		("f,function", "functions - evaluate, generate",
		 cxxopts::value<string>())
		("P,parallel", "number of tasks", cxxopts::value<int>());
	opt.parse_positional({"function"});
	auto result = opt.parse(argc, argv);
	if (result.count("help")) {
		cout << opt.help({"", "Group"}) << endl;
		return 0;
	}
	if (result.count("time"))
		thinktime = result["time"].as<int>();
	if (result.count("turns"))
		nsteps = result["turns"].as<int>();
	if (result.count("graph"))
		graph_fn = result["graph"].as<string>();
	if (result.count("games"))
		games = result["games"].as<int>();
	if (result.count("value_decay"))
		value_decay = result["value_decay"].as<float>();
	if (result.count("expfactor"))
		exploration_factor = result["expfactor"].as<float>();
	if (result.count("parallel"))
		nthreads = result["parallel"].as<int>();
	if (result.count("verbose"))
		verbose = true;
	if (result.count("function"))
		run = result["function"].as<string>();
	build_turn_table("rubik-turn-table.txt");

	try {
		function_map.at(run)();
	} catch(std::out_of_range &e) {
		cout << run << " is not a runable function" << endl;
	}

	return 0;
}
