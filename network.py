import numpy as np
import tensorflow as tf
import settings
import os
import cube as cb

class Network:
    def __init__(self, session, get_batch, train = True):
        self.is_train = train
        self.session = session
        self.get_batch = get_batch
        tf.train.get_or_create_global_step()
        self.init_network()
        self.saver = tf.train.Saver(self.savable_variables())
        if not os.path.exists(settings.FLAGS.model_dir):
            os.makedirs(settings.FLAGS.model_dir)
        self.path = settings.FLAGS.model_dir
        self.model_location = os.path.join(self.path, "model")
    def savable_variables(self):
        v = tf.trainable_variables()
        v.append(tf.train.get_global_step())
        return v
    def save(self):
        steps = self.session.run(tf.train.get_global_step())
        checkpoint = tf.train.latest_checkpoint(self.path)
        self.saver.save(self.session, self.model_location, steps)
        print("Saving parameters step {}".format(steps))
    def restore(self):
        latest_checkpoint = tf.train.latest_checkpoint(self.path)
        if latest_checkpoint:
            self.saver.restore(self.session, latest_checkpoint)
            steps = self.session.run(tf.train.get_global_step())
            print("Restore parameters {} step {}".format(
                latest_checkpoint, steps))
    def init_network(self):
        self.x = tf.identity(self.get_batch['cube'], name = "x")
        batch_size = tf.shape(self.x)[0]
        self.x = tf.layers.flatten(tf.one_hot(self.x, cb.Cube.nencodings))
        self.x = tf.cast(self.x, tf.float32)
        if self.is_train:
            self.y = tf.cast(self.get_batch['reward'], tf.float32, name = "y")
            self.step = tf.cast(self.get_batch['step'], tf.float32)

        self.net = tf.layers.dense(self.x,   4096, activation=tf.nn.relu)
        self.net = tf.layers.dense(self.net, 2048, activation=tf.nn.relu)
        self.net = tf.layers.dense(self.net, 512,  activation=tf.nn.relu)
        self.logits = tf.layers.dense(self.net, 1, activation=None)
        self.y_ = tf.reshape(self.logits, [-1], name="output")
        if self.is_train:
            loss_weights = tf.pow(settings.FLAGS.discount, self.step)
            #loss_weights = 1.0 / self.step
            self.loss = tf.losses.mean_squared_error(self.y, self.y_,
                    weights=loss_weights)
            self.optimizer = tf.train.RMSPropOptimizer(settings.FLAGS.learn_rate)
            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.control_dependencies(update_ops):
                self.train = self.optimizer.minimize(self.loss, global_step =
                    tf.train.get_global_step())
