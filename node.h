#pragma once
#include <vector>
#include <utility>
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <cassert>
#include <limits>
#include <memory>
#include <cubes.h>
#include <cmath>
#include <node.h>
#include <numeric>
#include <cassert>

extern float exploration_factor;

template<typename T, int N>
class node {
	private:
		std::vector<float> maxvalues;
		std::vector<int> visits;
		std::vector<T> states;
		std::vector<node<T, N>> children_nodes;
		void collect_sub(std::vector<int> &, std::vector<T *> &,
		    std::vector<float> &, T &);
		bool search_sub(std::vector<int> &, std::vector<T *> &, T &);
	public:
		node() {}
		node(T &s): maxvalues {1, std::numeric_limits<float>::lowest()},
			visits {1, 0}, states {1, s}, children_nodes {1} {}
		T &findbest() {
			float max_value = std::numeric_limits<float>::lowest();
			int index = 0;
			for (auto i = 0; i < children_nodes.size(); i ++) {
				if (maxvalues[i] > max_value) {
					max_value = maxvalues[i];
					index = i;
				}
			}
			if (children_nodes[index].expanded())
				return children_nodes[index].findbest();
			else
				return states[index];
		}
		bool expand_stage_1(const T &);
		float expand_stage_2(std::vector<float>);
		bool expanded() const { return !children_nodes.empty(); }
		int selection(std::vector<T *> &);
		std::vector<T> &get_states() {
			return states;
		}
		std::vector<node<T, N>> &get_children_nodes() {
			return children_nodes;
		}
		std::vector<float> &get_maxvalues() {
			return maxvalues;
		}
		template<typename S, int I>
		friend std::ostream & operator << (std::ostream &,
				const node<S, I> &);
		void output(std::ostream &, int) const;
		void stats(std::vector<float> &, std::vector<int> &, int);
		bool search(std::vector<int> &, std::vector<T *> &);
		void collect(std::vector<int> &, std::vector<T *> &,
		    std::vector<float> &);
};

template <typename T, int N>
void node<T, N>::stats(std::vector<float> &value, std::vector<int> &stack, int level)
{
	while (stack.size() < level + 1) {
		stack.push_back(0);
		value.push_back(std::numeric_limits<float>::lowest());
	}
	stack[level] ++;
	auto x = max_element(maxvalues.begin(), maxvalues.end());
	if (*x > value[level])
		value[level] = *x;
	for (auto &x : children_nodes) {
		if (!x.expanded())
			continue;
		x.stats(value, stack, level + 1);
	}
}

template<typename T, int N>
void node<T, N>::output(std::ostream &os, int level) const
{
	for (auto i = 0; i < children_nodes.size(); i ++) {
		for (auto indent = 0; indent < level * 2; indent ++)
			std::cout << " ";
		std::cout << "[" << i << "] "<< " " << maxvalues[i]
			<< " " << visits[i] << std::endl;
		if (children_nodes[i].expanded())
			children_nodes[i].output(os, level + 2);
	}
}

template<typename T, int N>
std::ostream & operator << (std::ostream &os, const node<T, N> &n)
{
	n.output(os, 0);
	return os;
}

template<typename T, int N>
void node<T, N>::collect_sub(std::vector<int> &path, std::vector<T *> &p_sts,
    std::vector<float> &value, T & parent_state)
{
	if (!expanded())
		return;
	for (auto child = 0; child < children_nodes.size(); child ++) {
		path.push_back(child);
		value.push_back(maxvalues[child]);
		p_sts.push_back(&parent_state);
		children_nodes[child].collect_sub(path, p_sts, value,
				states[child]);
	}
}

template<typename T, int N>
void node<T, N>::collect(std::vector<int> &path, std::vector<T *> &p_sts,
    std::vector<float> &value)
{
	if (!expanded())
		return;
	assert(children_nodes.size() == 1);
	children_nodes[0].collect_sub(path, p_sts, value, states[0]);
}

template<typename T, int N>
bool node<T, N>::search(std::vector<int> &path, std::vector<T *> &p_sts)
{
	if (!expanded())
		return false;
	assert(children_nodes.size() == 1);
	if (states[0].solved())
		return true;
	return children_nodes[0].search_sub(path, p_sts, states[0]);
}

template<typename T, int N>
bool node<T, N>::search_sub(std::vector<int> &path, std::vector<T *> &p_sts,
    T & parent_state)
{
	for (auto child = 0; child < children_nodes.size(); child ++) {
		p_sts.push_back(&parent_state);
		path.push_back(child);
		if (states[child].solved())
			return true;
		if (!children_nodes[child].search_sub(path, p_sts,
		    states[child])) {
			path.pop_back();
			p_sts.pop_back();
		} else
			return true;
	}
	return false;
}

template<typename T, int N>
bool node<T, N>::expand_stage_1(const T &state)
{
	bool r = false;
	int offset = 0;
	assert(states.empty());
	states.resize(N);
	for (auto i = 0; i < N; i ++) {
		states[offset] = state;
		states[offset].turn(i);
		if (states[offset].solved())
			r = true;
		offset ++;
	}
	return r;
}

template<typename T, int N>
float node<T, N>::expand_stage_2(std::vector<float> values)
{
	visits.resize(N, 1);
	children_nodes.resize(N);
	maxvalues = values;
	return *std::max_element(values.begin(), values.end());
}

template<typename T, int N>
int node<T, N>::selection(std::vector<T *> &path)
{
	assert(expanded());
	int total_visits = accumulate(visits.begin(), visits.end(), 0);
	int index = 0;
	float max_value = std::numeric_limits<float>::lowest();
	for (auto i = 0; i < children_nodes.size(); i ++) {
		for (auto x : path)
			if (*x == states[i])
				continue;
		float value = exploration_factor *
			sqrt(log(total_visits) / (visits[i] - 0.99));
		value += maxvalues[i];
		if (value > max_value) {
			index = i;
			max_value = value;
		}
	}
	visits[index] ++;
	return index;
}
