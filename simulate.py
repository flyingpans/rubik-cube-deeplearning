from gen_sequence import generate_sequence
import tensorflow as tf
import os
import sys
import numpy as np
import argparse
from network import Network
import cube as cb
import settings

FLAGS=None

def _float32_feature(value):
    """Wrapper for inserting an float32 Feature into a SequenceExample proto,
    """
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))

def _int64_feature(value):
    """Wrapper for inserting an int64 Feature into a SequenceExample proto,
    e.g, An integer label.
    """
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def _bytes_feature(value):
    """Wrapper for inserting a bytes Feature into a SequenceExample proto,
    e.g, an image in byte
    """
    # return tf.train.Feature(bytes_list=tf.train.BytesList(value=[str(value)]))
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def _int64_feature_list(values):
    """Wrapper for inserting an int64 FeatureList into a SequenceExample proto,
    e.g, sentence in list of ints
    """
    return tf.train.FeatureList(feature=[_int64_feature(v) for v in values])

def _bytes_feature_list(values):
    """Wrapper for inserting a bytes FeatureList into a SequenceExample proto,
    e.g, sentence in list of bytes
    """
    return tf.train.FeatureList(feature=[_bytes_feature(v) for v in values])

def simulate_one(session):
    next_element = dict()
    next_element['cube'] = tf.placeholder(tf.int64, [None, cb.Cube.cubes])
    network = Network(session, next_element, False)
    writer = tf.python_io.TFRecordWriter(settings.FLAGS.tfrecord)
    session.run(tf.global_variables_initializer())
    network.restore()

    for iterations in range(settings.FLAGS.simulations):
        if iterations % 1000 == 0:
            print("{:4d} iterations".format(iterations))
            sys.stdout.flush()
        episode = generate_sequence()
        for cube, action, seqno, subseq, rewards in episode:
            assert(np.all(cube < 24) and np.all(cube >= 0) and
                    cube.shape[0] == 20)
            x = np.reshape(subseq, [-1, cb.Cube.cubes])
            y, = session.run([network.y_], feed_dict = { next_element['cube']: x })
            y += 1.0 * rewards
            y = np.max(y)
            feature = {
              'training/cube': tf.train.Feature(
                  int64_list=tf.train.Int64List(value=cube.tolist())),
              'training/action': _int64_feature(action),
              'training/steps': _int64_feature(seqno),
              'training/reward': _float32_feature(y) }
            example = tf.train.Example(features = tf.train.Features(feature = feature))
            writer.write(example.SerializeToString())
    writer.close()
    sys.stdout.flush()

def main(unused_argv):
    with tf.Session() as session:
        simulate_one(session)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--model_dir', type = str,
        default = '/tmp/models',
        help = 'model directory')
    parser.add_argument(
        '--tfrecord', type = str,
        default = '/tmp/rubik-cube-sim',
        help = 'tfrecord of rubik cube simulations')
    parser.add_argument(
        '--simulations', type = int,
        default = 10000,
        help = 'number of simulations')
    settings.FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main = main, argv = [sys.argv[0]] + unparsed)
