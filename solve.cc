#include <tensorflow/core/protobuf/meta_graph.pb.h>
#include <tensorflow/core/public/session.h>
#include <tensorflow/core/public/session_options.h>
#include <tensorflow/cc/ops/standard_ops.h>
#include <iostream>
#include <string>
#include <cstdint>
#include <cstddef>
#include <cstdlib>
#include <cassert>
#include <random>
#include <functional>
#include <limits>
#include <cstring>
#include <csv-parser.h>
#include <algorithm>
#include <cxxopts.h>
#include <cubeconst.h>
#include <cubes.h>
#include <memory>

using namespace std;
using namespace tensorflow;
using namespace tensorflow::ops;

typedef vector<pair<string, Tensor>> tensor_dict;

Status LoadModel(Session *sess, string graph_fn, string checkpoint_fn = "")
{
	Status status;

	// Read in the protobuf graph we exported
	GraphDef graph_def;
	status = ReadBinaryProto(Env::Default(), graph_fn, &graph_def);
	if (status != Status::OK()) return status;

	// create the graph in the current session
	status = sess->Create(graph_def);
	if (status != Status::OK()) return status;
	return Status::OK();
}

int depth = 2;

int nleaves()
{
	int n = 1;
	for (auto i = 0; i < depth; i ++)
		n *= nactions;
	return n;
}

bool traverse(const Cube &cube, Tensor &x, int &offset, int level)
{
	auto mapped_x = x.tensor<int64, 2>();
	if (level == 0)
		return false;
	for (auto i = 0; i < nactions; i ++) {
		auto nxt = cube;
		nxt.turn(i);
		if (nxt.solved())
			return true;
		if (traverse(nxt, x, offset, level - 1) == true)
			return true;
		if (level == 1) {
			const auto nxt_state = nxt.encode();
			for (auto j = 0; j < x.dim_size(1); j ++)
				mapped_x(offset, j) = nxt_state[j];
			offset ++;
		}
	}
	return false;
}

const int batch_size = 256;

bool solve_one(Session *session, auto &dice, int nsteps, int iter)
{
	Cube cube;
	int seqno = 0;
	for (auto step = 0; step < nsteps; step ++) {
		auto action_index = dice();
		cube.turn(action_index);
	}
	for (auto step = 0; step < nsteps / depth + 1; step ++) {
		auto x = Tensor(DT_INT64, TensorShape({nleaves(), cubes}));
		int offset = 0;
		if (traverse(cube, x, offset, depth))
			return true;
		int batch = nleaves();
		offset = 0;
		float minimum = numeric_limits<float>::max();
		int min_index = 0;
		while (batch > 0) {
			int sz = batch > batch_size ? batch_size : batch;
			auto slice = x.Slice(offset, offset + sz);
			tensor_dict feed_dict = { {"x", slice} };
			vector<Tensor> output;
			TF_CHECK_OK(session->Run(feed_dict, {"output"}, {},
						&output));
			auto mapped_y = output[0].tensor<float, 1>();
			for (auto i = 0; i < sz; i ++) {
				if (mapped_y(i) < minimum) {
					minimum = mapped_y(i);
					min_index = offset + i;
				}
			}
			batch -= sz;
			offset += sz;
		}
		vector<int> turns;
		for (auto i = 0; i < depth; i ++) {
			turns.push_back(min_index % nactions);
			min_index /= nactions;
		}
		for (auto i = depth - 1; i >= 0; i --)
			cube.turn(turns[i]);
	}
	return false;
}

float solve_many(int nsteps, int iterations, const string &graph_fn)
{
	Session *session;
	SessionOptions options;
	default_random_engine generator;
	uniform_int_distribution<int> distribution(0, nactions - 1);
	auto dice = bind(distribution, generator);

	TF_CHECK_OK(NewSession(options, &session));
	TF_CHECK_OK(LoadModel(session, graph_fn));

	auto solved = 0;
	for (auto i = 0; i < iterations; i ++) {
		if (solve_one(session, dice, nsteps, i))
			solved ++;
	}
	return (float)solved / iterations;
}

int main(int argc, char **argv)
{
	int nsteps = 10;
	int games = 1000;
	string graph_fn = "./models/model.pb";
	cxxopts::Options opt("solve", "Rubik cube deep learning solver");
	opt.add_options()
		("t,turns", "number of shuffles", cxxopts::value<int>())
		("g,graph", "locations of the model", cxxopts::value<string>())
		("p,games", "number of games", cxxopts::value<int>())
		("d,depth", "search depth", cxxopts::value<int>());
	auto result = opt.parse(argc, argv);
	if (result.count("depth"))
		depth = result["depth"].as<int>();
	if (result.count("turns"))
		nsteps = result["turns"].as<int>();
	if (result.count("graph"))
		graph_fn = result["graph"].as<string>();
	if (result.count("games"))
		games = result["games"].as<int>();
	build_turn_table("rubik-turn-table.txt");

	float rate = solve_many(nsteps, games, graph_fn);

	cout << rate << endl;

	return 0;
}
