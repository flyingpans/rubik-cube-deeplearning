import numpy as np
import tensorflow as tf
import cube as cb
import settings
import os
import sys
import argparse
import glob
import settings
import random
from network import Network

def main(unused_argv):
    next_element = dict()
    with tf.Session() as session:
        if settings.FLAGS.model != '':
            path = os.path.join(settings.FLAGS.model_dir, settings.FLAGS.model)
            with tf.gfile.GFile(path, 'rb') as f:
                graph_def = tf.GraphDef()
                graph_def.ParseFromString(f.read())
                tf.import_graph_def(graph_def, name="")
            x = tf.get_default_graph().get_tensor_by_name("x:0")
            y_ = tf.get_default_graph().get_tensor_by_name("output:0")
            print("Loading models")
        else:
            next_element['cube'] = tf.placeholder(tf.int64, [None, cb.Cube.cubes])
            x = next_element['cube']
            next_element['steps'] = tf.placeholder(tf.float32, [None])
            next_element['action'] = tf.placeholder(tf.float32, [None])
            network = Network(session, next_element, train=False)
            y_ = network.y_
        session.run(tf.global_variables_initializer())
        if settings.FLAGS.model == '':
            network.restore()

        solved = 0
        total  = 0
        for _ in range(1000):
            cube = cb.Cube()
            seqno = 0
            actions = []
            for step in range(settings.FLAGS.turns):
                action_index = random.randint(0, len(settings.actions) - 1)
                action = settings.actions[action_index]
                actions.append(action)
                cube.turn(action)
            total += 1
            for step in range(settings.FLAGS.turns):
                state = np.zeros([len(settings.actions), cb.Cube.cubes],
                    dtype=np.int64)
                for i,a in enumerate(settings.actions):
                    nxt = cube.clone()
                    nxt.turn(a)
                    nxt_state = nxt.encode()
                    state[i] = nxt_state
                values, = session.run([y_], feed_dict = { x : state } )
                values = np.reshape(values, [-1])
                action_index = np.argmin(values)
                action = settings.actions[action_index]
                cube.turn(action)
                if cube.solved():
                    solved += 1
                    break
    print("total = {}, solved = {}, rate {}%".format(total, solved,
        solved/total))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
            '--model_dir', type = str,
            default = '/tmp/models',
            help = 'model directory')
    parser.add_argument(
            '--model', type = str,
            default = '',
            help = 'model file')
    parser.add_argument(
            '--turns', type = int,
            default = 6,
            help = 'generate number of turns')
    settings.FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main = main, argv = [sys.argv[0]] + unparsed)
