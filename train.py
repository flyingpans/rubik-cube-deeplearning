import numpy as np
import tensorflow as tf
import cube as cb
import settings
import os
import sys
import argparse
import glob
import settings
from network import Network
from tensorflow.python.framework import graph_util
from tensorflow.python.framework import ops

def tfrecord_parser(record):
    features = tf.parse_single_example(record,
            features = {
                'training/step': tf.FixedLenFeature([], tf.int64),
                'training/cube': tf.FixedLenFeature([cb.Cube.cubes],tf.int64),
                'training/reward': tf.FixedLenFeature([], tf.float32)})
    step  = tf.cast(features['training/step'], tf.int32)
    cube = features['training/cube']
    reward = tf.cast(features['training/reward'], tf.float32)
    return { 'cube': cube, 'step': step, 'reward': reward }

def create_empty_network(session):
    next_element = dict()
    next_element['cube'] = tf.placeholder(tf.int64, [None, cb.Cube.cubes])
    network = Network(session, next_element, False)
    session.run(tf.global_variables_initializer())
    return network

def main(unused_argv):
    if settings.FLAGS.init:
        with tf.Session() as session:
            network = create_empty_network(session)
            network.save()
            return
    if settings.FLAGS.save_graph:
        with tf.Session() as session:
            network = create_empty_network(session)
            network.restore()
            model_file = os.path.join(settings.FLAGS.model_dir, "model.pb")
            output_node_names = "output"
            output_graph_def = graph_util.convert_variables_to_constants(
                session,
                tf.get_default_graph().as_graph_def(),
                output_node_names.split(","))
            with tf.gfile.GFile(model_file, "wb") as f:
                f.write(output_graph_def.SerializeToString())
            return
    filenames = glob.glob(os.path.join(settings.FLAGS.tfrecord, "*.tfrecord"))
    iterator = (tf.data.TFRecordDataset(filenames)
            .map(tfrecord_parser)
            .batch(settings.FLAGS.batch_size)
            .shuffle(buffer_size = settings.FLAGS.shuffle_buffer_size)
            .prefetch(16)
            .make_initializable_iterator())
    next_element = iterator.get_next()
    with tf.Session() as session:
        network = Network(session, next_element)
        session.run(tf.global_variables_initializer())
        network.restore()
        if settings.FLAGS.print_variables:
            vars = tf.get_default_graph().as_graph_def().node
            for var in vars:
                print(var.name)
            return
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        epoch = 0
        while epoch < settings.FLAGS.epochs:
            print("EPOCH {}".format(epoch))
            sys.stdout.flush()
            session.run(iterator.initializer)
            acc_loss = 0.0
            while True:
                try:
                    step_var = tf.train.get_global_step()
                    _, loss, steps= session.run([network.train, network.loss,
                        step_var])
                    acc_loss += loss
                    if steps % 100 == 0:
                        print("{} {:.2f} {}".format(epoch,acc_loss / 100,steps))
                        acc_loss = 0.0
                        sys.stdout.flush()
                    if steps % 1000 == 0:
                        network.save()
                        sys.stdout.flush()
                except tf.errors.OutOfRangeError:
                    break
            epoch += 1

        coord.request_stop()
        coord.join(threads)

if __name__ == '__main__':
    np.set_printoptions(suppress = True)
    parser = argparse.ArgumentParser()
    parser.add_argument(
            '--tfrecord', type = str,
            default = '/tmp/',
            help = 'tfrecord directory')
    parser.add_argument(
            '--model_dir', type = str,
            default = '/tmp/models',
            help = 'model directory')
    parser.add_argument(
            '--learn_rate', type = float,
            default = 0.0001,
            help = 'learning rate')
    parser.add_argument(
            '--shuffle_buffer_size', type = int,
            default = 10000,
            help = 'shuffle buffer size')
    parser.add_argument(
            '--batch_size', type = int,
            default = 128,
            help = 'mini batch size')
    parser.add_argument(
            '--init', type = bool,
            default = False,
            help = 'initialize and exit')
    parser.add_argument(
            '--print_variables', type = bool,
            default = False,
            help = 'print variables and exit')
    parser.add_argument(
            '--save_graph', type = bool,
            default = False,
            help = 'save graphs')
    parser.add_argument(
            '--discount', type = float,
            default = 0.90,
            help = 'training discount')
    parser.add_argument(
            '--epochs', type = int,
            default = 1,
            help = 'epochs')
    settings.FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main = main, argv = [sys.argv[0]] + unparsed)
